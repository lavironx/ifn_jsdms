---
title: Supplementary material
output:
  pdf_document:
    fig_width: 4
    fig_height: 4
    fig_caption: false
---

```{r knit_options, echo=FALSE}
knitr::opts_knit$set(root.dir = "../")
knitr::opts_knit$set(knitr.duplicate.label = "allow")
options(knitr.duplicate.label = "allow")
# options(digits = 2)
```

```{r setup, echo=FALSE}
source("config.R")
source("R/utils.R")
source("R/generate_figures.R")

library(knitr)
library(ggplot2)
library(raster)
library(magrittr)
library(reshape2)
library(gridExtra)
library(grid)
library(xtable)
```

```{r read_data, echo=F}
cache.eval <- recover_cache(path = ".drake_eval")

# load data from drake cache
loadd(IfnNewMethod_FullData, cache = cache.eval)
loadd(IfnNewMethod_SubsetData, cache = cache.eval)
loadd(IfnNewMethod_evalconv_merged, cache = cache.eval)
loadd(IfnNewMethod_evalpred_merged, cache = cache.eval)
loadd(IfnNewMethod_evalpred_BIOMOD_merged, cache = cache.eval)
loadd(IfnNewMethod_evalpred_comm, cache = cache.eval)
loadd(IfnNewMethod_evalpred_comm_biomod, cache = cache.eval)
loadd(IfnNewMethod_evalpred_compo, cache = cache.eval)
loadd(IfnNewMethod_evalpred_compo_biomod, cache = cache.eval)
loadd(IfnNewMethod_params_merged, cache = cache.eval)
loadd(IfnNewMethod_params_values_BIOMOD, cache = cache.eval)
loadd(IfnNewMethod_responses, cache = cache.eval)
loadd(IfnNewMethod_responses_BIOMOD, cache = cache.eval)

loadd("plot_map_evaluations", cache = cache.eval)
loadd("plot_sp_selection_suppmat", cache = cache.eval)
loadd("plot_rare_species", cache = cache.eval)
loadd("plot_predictions", cache = cache.eval)
loadd("plot_communities", cache = cache.eval)

map_theme <-
  theme(axis.text         = element_blank(),
        axis.ticks        = element_blank(),
        panel.grid.major  = element_blank(),
        panel.grid.minor  = element_blank(),
        panel.border      = element_blank(),
        panel.background  = element_blank(),
        strip.text        = element_text(face = "bold"),
        strip.background  = element_rect(fill = "white", colour = "white"))
```

## Data

We used data from the French National Forest Inventory.


## Data pre-processing

```{r , echo=F}
df.occur <- IfnNewMethod_FullData$occur
```


Some processing have been done on the data to exclude species/plots known to be
too difficult to modelize.
This processing consits mainly in the following steps (see function
`format_IfnNewMethod()` in `R/format_functions.R` for details on the
processing):

- Remove plots containing exotic or cultivated species (based on label found in
  NFI data)
- Remove plots containing very rare species (species present in less than 5% of
  plots)
- Remove plots containing shrub species (life form from TRY databse, only one
  species found: _Corylus avellana_)

There remains a total of `r nrow(df.occur)` plots and `r ncol(df.occur)`
species.

```{r, echo=F, results="asis"}
vars <- IfnNewMethod_FullData$env
colnames(vars) <- c("GDD5", "WB", "SLOPE", "PREC", "PH")
a <- cor(vars)
a <- round(a, 2)
a[upper.tri(a)] <- "-"
b <- xtable(a, digits = 2)
# print(b, type = "latex", comment = F)
kable(a)
```
