# Evaluation of JSDMs on NFI data

## About this project

This project contains code for the evaluation of some JSDMs implementations on
the french National Forest Inventory data.


## Run the project

The analyses workflow is managed using `drake`.
You need the latest version of `drake` from
[github](https://github.com/ropensci/drake).
Then, just run the script `make.R` to reproduce the workflow.
